package com.itheima.test;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class POITest {
    //使用POI读取Excel文件中的数据
//    @Test
    public void test1() throws Exception{
        //加载指定文件，创建一个Excel对象（工作簿）
        XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File("d:\\poi.xlsx")));
        //读取Excel文件中第一个Sheet标签页
        XSSFSheet sheet = excel.getSheetAt(0);
        //遍历Sheet标签页，获得每一行数据
        for (Row row : sheet) {
            //遍历行，获得每个单元格对象
            for (Cell cell : row) {
                System.out.println(cell.getStringCellValue());
            }
        }
        //关闭资源
        excel.close();
    }

    //使用POI读取Excel文件中的数据
//    @Test
    public void test2() throws Exception{
        //加载指定文件，创建一个Excel对象（工作簿）
        XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File("d:\\poi.xlsx")));
        //读取Excel文件中第一个Sheet标签页
        XSSFSheet sheet = excel.getSheetAt(0);
        //获得当前工作表中最后一个行号，需要注意：行号从0开始
        int lastRowNum = sheet.getLastRowNum();
        System.out.println("lastRowNum = " + lastRowNum);
        /*注意点：在工作薄中，一个两行两列的表，遍历的最后一行索引是‘1’，遍历的最后一列的索引是‘2’
        *           所以用‘<=’遍历行索引[0,1]  ,用‘<’遍历列索引[0,1]
        * */
//      遍历行，使用“<=” i<=lastRowNum  *************************************************************
        for(int i=0;i<=lastRowNum;i++){
            XSSFRow row = sheet.getRow(i);//根据行号获取每一行
            //获得当前行最后一个单元格索引
            short lastCellNum = row.getLastCellNum();
            System.out.println("lastCellNum = " + lastCellNum);
 //      遍历了列，使用“<” j<lastCellNum   *************************************************************
            for(int j=0;j<lastCellNum;j++){
                XSSFCell cell = row.getCell(j);//根据单元格索引获得单元格对象
                System.out.println(cell.getStringCellValue());
            }
        }
        //关闭资源
        excel.close();
    }

    //使用POI向Excel文件写入数据，并且通过输出流将创建的Excel文件保存到本地磁盘
//    @Test
    public void test3() throws Exception{
        //在内存中创建一个Excel文件（工作簿）
        XSSFWorkbook excel = new XSSFWorkbook();
        //创建一个工作表对象
        XSSFSheet sheet = excel.createSheet("传智播客");
        //在工作表中创建行对象
        XSSFRow title = sheet.createRow(0);
        //在行中创建单元格对象
        title.createCell(0).setCellValue("姓名");
        title.createCell(1).setCellValue("地址");
        title.createCell(2).setCellValue("年龄");

        XSSFRow dataRow = sheet.createRow(1);
        dataRow.createCell(0).setCellValue("小明");
        dataRow.createCell(1).setCellValue("北京");
        dataRow.createCell(2).setCellValue("20");

        //创建一个输出流，通过输出流将内存中的Excel文件写到磁盘
        FileOutputStream out = new FileOutputStream(new File("d:\\poi.xlsx"));
        excel.write(out);
        out.flush();
        excel.close();
    }


    //使用POI向Excel文件写入数据，使用模板
//    @Test
    public void test4() throws Exception{
        //在内存中创建一个Excel文件（工作簿）
        XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File("d:\\poi.xlsx")));
        //创建一个工作表对象
        XSSFSheet sheet = excel.getSheetAt(0);//拿到第一张表对象
        //在工作表中创建行对象
        XSSFRow row = sheet.getRow(0);//拿到第一行对象
        /*XSSFCellStyle rowStyle = row.getRowStyle();//拿到第一行的格式
        System.out.println(rowStyle);*/
        XSSFCellStyle cellStyle = row.getCell(0).getCellStyle();//拿到第一行第一列的格式
        System.out.println(cellStyle);


        XSSFRow dataRow = sheet.createRow(4);//创建新的一行，第四行
//        dataRow.setRowStyle(rowStyle);//给此行设置和第一行相同的格式
        //给此行创建单元格 并赋值
        dataRow.createCell(0).setCellValue("小明");
        dataRow.createCell(1).setCellValue("北京");
        dataRow.createCell(2).setCellValue("20");

        for (Cell cell : dataRow) {
            cell.setCellStyle(cellStyle);//设置单元格格式

        }
        //创建一个输出流，通过输出流将内存中的Excel文件写到磁盘
        FileOutputStream out = new FileOutputStream(new File("d:\\poi_4.xlsx"));
        excel.write(out);
        out.flush();
        excel.close();
    }
}
