package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户操作
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @Reference
    private UserService userService;


    /**
     * 获得当前登录用户的用户名
     * @return
     */
    @RequestMapping("/getUsername")
    public Result getUsername(){
        //当Spring security完成认证后，会将当前用户信息保存到框架提供的上下文对象
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(user);
        if(user != null){
            String username = user.getUsername();
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,username);
        }

        return new Result(false, MessageConstant.GET_USERNAME_FAIL);
    }

    /**
     * 查询所有
     * @return
     */
    @RequestMapping("/findPage.do")
    public PageResult findAll(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = userService.pageQuery(queryPageBean);
        return pageResult;
    }

    /**
     * 添加用户
     * @param user
     * @param ids
     * @return
     */
    @RequestMapping("/add.do")
    public Result add(@RequestBody com.itheima.pojo.User user, int[] ids) {

        try {
            String pass = "123456";
            BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
            String hashPass = bcryptPasswordEncoder.encode(pass);
            user.setPassword(hashPass);

            userService.add(user, ids);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "添加用户失败");
        }
            return  new Result(true, "添加用户成功");

    }

    /**
     * 根据id查询用户
     * @param id
     * @return
     */
    @RequestMapping("/findById.do")
    public Result findById(int id){
        com.itheima.pojo.User user=null;
        List<Integer> list =null;
        try {
             user=userService.findById(id);
             list=userService.findRoleById(id);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "查询失败");
        }
            return  new Result(true, "查询成功",user,list);

    }

    /**
     * 编辑User信息  同时关联角色信息
     * @param ids
     * @param user
     * @return
     */
    @RequestMapping("/edit.do")
    public Result edit(@RequestParam(value = "ids")Integer[] ids, @RequestBody com.itheima.pojo.User user){

        try {
            userService.edit(ids,user);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "编辑失败!!!");
        }
        return new Result(true,"编辑成功!!!");
    }

    /**
     * 删除用户
     * @param id
     * @return
     */
    @RequestMapping("/delete.do")
    public Result delete(@RequestParam(value = "id")Integer id){

        try {
            userService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "删除用户失败!!!");
        }
        return new Result(true,"删除用户成功!!!");
    }
}
