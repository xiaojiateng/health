package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.ShowOrder;
import com.itheima.service.MemberService;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    /**
     * 分页查询预约记录
     * @param queryPageBean
     * @return
     */
//    显示请求列表=>查询预约
    @RequestMapping("/showOrderByPage.do")
    public PageResult showOrder(@RequestBody QueryPageBean queryPageBean){  //新建一个实体类ShowOrder展示数据
        return orderService.findPage(queryPageBean);

    }

    /**
     * 确定预约
     * @param id
     * @return
     */
//
    @RequestMapping("/sendSuccessMessage.do")
    public Result sendSuccessMessage(int id){
//        System.out.println(id); 接收到了
        try {
            orderService.sendMessage(id,"success");
            return new Result(true,"通知短信发送成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,"通知短信发送失败");
    }

    /**
     * 新增预约
     * @param showOrder
     * @param setmealId
     * @return
     */

    //
    @RequestMapping("/add.do")
    public Result getAllSetmeal(@RequestBody ShowOrder showOrder, int setmealId) {
        Order newOrder = new Order();   //新建请求把参数封装进去
        newOrder.setOrderDate(showOrder.getOrderDate());
        newOrder.setOrderType(showOrder.getOrderType());
        newOrder.setOrderStatus(showOrder.getOrderStatus());
        newOrder.setSetmealId(setmealId);
        //调用体检预约服务
        try {
            return orderService.orderByComputer(newOrder,showOrder.getPhoneNumber(),showOrder.getName());//通过手机号找到会员id
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false,"新增请求失败");
        }
    }

    /**
     * 取消预约
     * @param id
     * @return
     */
//
    @RequestMapping("/cancel.do")
    public Result cancel(int id){
//        System.out.println(id); 接收到了
        try {
            orderService.sendMessage(id,"failure"); //发送失败结果没有输出
            orderService.cancel(id);
            return new Result(true,"取消预约成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,"取消预约失败");
    }

    /**
     * 删除选中
     * @param ids
     * @return
     */
//
    @RequestMapping("/deleteIds.do")
    public Result deleteMany(@RequestBody int[] ids){
        try {
            for (int id : ids) {
                cancel(id);
            }
            return new Result(true,"删除选中成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,"删除选中失败");
    }

}
