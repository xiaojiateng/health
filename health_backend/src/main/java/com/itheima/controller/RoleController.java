package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Role;
import com.itheima.service.CheckGroupService;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/role")
public class RoleController {


    @Reference
    private RoleService roleService;


    /**
     * 角色分页查询
     * @param queryPageBean
     * @return
     */

    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = roleService.pageQuery(queryPageBean);
        return pageResult;

    }

    /**
     * 添加角色
     * @param role
     * @param ids
     * @return
     */
    //    "/role/add.do",this.formData
    @RequestMapping(path = "/add.do")
    public Result add(@RequestBody Role role, int[] ids) {

        try {
            roleService.add(role, ids);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "添加失败");
        }
            return  new Result(true, "添加成功");

    }

    /**
     * 查询单个角色
     * @param id
     * @return
     */
    //    "/role/findById.do?id=" + row.id
    @RequestMapping(path = "/findById.do")
    public Result findById(int id) {
        Role role =null;
        try {
             role=roleService.findById(id);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "查询失败");
        }
            return  new Result(true, "查询成功", role);

    }

    /**
     * 编辑角色
     * @param role
     * @param ids
     * @return
     */
    //    "/role/edit.do",this.formData
    @RequestMapping(path = "/edit.do")
    public Result edit(@RequestBody Role role, int[] ids) {

        try {
            roleService.edit(role, ids);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "编辑失败");
        }
            return  new Result(true, "编辑成功");

    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    //    "/role/delete.do?id="+row.id
    @RequestMapping(path = "/delete.do")
    public Result delete(int id) {

        try {
            roleService.delete(id);

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "删除角色失败");
        }
            return  new Result(true, "删除角色成功");

    }


    /**
     * 查询所有角色
     * @return
     */
    //findAll
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {
        List<Role> list=null;
        try {
            list=roleService.findAll();

        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "角色查询失败");
        }
            return  new Result(true, "角色查询成功",list);

    }
}
