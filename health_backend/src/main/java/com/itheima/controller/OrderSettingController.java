package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.CheckGroupService;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.POIUtils;
import com.sun.jersey.api.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/ordersetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;

    //    /ordersetting/editNumberByDate.do
    @RequestMapping(path = "/editNumberByDate.do")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting) {

        System.out.println("OrderSetting参数为 ： " + orderSetting);
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDERSETTING_FAIL);
        }

    }


    //  /ordersetting/getOrderSettingByMonth.do
    @RequestMapping(path = "/getOrderSettingByMonth.do")
    public Result getOrderSettingByMonth(String date) {

        System.out.println("时间参数为 ： " + date);
        try {
            List<Map> list = orderSettingService.getOrderSettingByMonth(date);
            return new Result(true, MessageConstant.GET_ORDERSETTING_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_ORDERSETTING_FAIL);
        }

    }


    //  /ordersetting/upload.do
    @RequestMapping(path = "/upload.do")
    public Result upload(@RequestParam("excelFile") MultipartFile excelFile) {

        try {
            List<String[]> readExcel = POIUtils.readExcel(excelFile);
            ArrayList<OrderSetting> list = new ArrayList<>();
            for (String[] strings : readExcel) {
                String orderDate = strings[0];
                String number = strings[1];
                OrderSetting orderSetting = new OrderSetting(new Date(orderDate), Integer.parseInt(number));
                list.add(orderSetting);
            }

            orderSettingService.add(list);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
        return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);

    }


}
