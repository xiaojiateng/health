package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/checkitem")
public class CheckItemController {


    @Reference
    private CheckItemService checkItemService;

    //    findAll
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {
        List<CheckItem> list =null;
        try {
            list=checkItemService.findAll();

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);
        }
       return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS, list);

    }

    @PreAuthorize("hasAuthority('CHECKITEM_EDIT')")//编辑权限校验
    @RequestMapping(path = "/edit.do")
    public Result edit(@RequestBody CheckItem checkItem) {

        try {
            checkItemService.edit(checkItem);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);

        }
        return new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);

    }

    @RequestMapping(path = "/findById.do")
    public Result findById(int id) {
        CheckItem checkItem =null;
        try {
             checkItem=checkItemService.findById(id);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKITEM_FAIL);

        }
        return new Result(true, MessageConstant.QUERY_CHECKITEM_SUCCESS, checkItem);

    }


    //删除检查项
    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping(path = "/delete.do")
    public Result delete(int id) {

        try {
            checkItemService.delete(id);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);

        }
        return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);

    }

    @PreAuthorize("hasAuthority('CHECKITEM_ADD')")//新增权限校验
    @RequestMapping(path = "/add.do")
    public Result add(@RequestBody CheckItem checkItem) {

        try {
            checkItemService.add(checkItem);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);

        }
        return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);

    }


    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")//权限校验
    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {


        System.out.println(queryPageBean);
        PageResult pageResult = checkItemService.findByPage(queryPageBean);

        return pageResult;

    }

}
