package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.service.PermissionService;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/permission")
public class PermissionController {


    @Reference
    private PermissionService permissionService;

    /**
     * 权限删除
     *
     * @param id
     * @return
     */
    //    "/permission/delete.do?id="+row.id
    @RequestMapping(path = "/delete.do")
    public Result delete(int id) {

        try {
            permissionService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "权限删除失败");

        }

        return new Result(true, "权限删除成功");

    }


    /**
     * 权限编辑
     *
     * @param permission
     * @return
     */
    //    "/permission/edit.do",this.formData
    @RequestMapping(path = "/edit.do")
    public Result edit(@RequestBody Permission permission) {

        try {
            permissionService.edit(permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "编辑权限失败");

        }
            return new Result(true, "编辑权限成功");


    }

    /**
     * 单个权限查询
     *
     * @param id
     * @return
     */
    //    "/permission/findById.do?id="+row.id
    @RequestMapping(path = "/findById.do")
    public Result findById(int id) {
        Permission permission =null;
        try {
             permission=permissionService.findById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询权限失败");

        }
            return new Result(true, "查询权限成功", permission);


    }


    /**
     * 添加权限
     *
     * @param permission
     * @return
     */
    //    "/permission/add.do",this.formData
    @RequestMapping(path = "/add.do")
    public Result add(@RequestBody Permission permission) {

        try {
            permissionService.add(permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "添加权限失败");

        }
            return new Result(true, "添加权限成功");


    }

    /**
     * 权限记录分页
     *
     * @param queryPageBean
     * @return
     */
    //    "/permission/findPage.do",param
    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {


        System.out.println(queryPageBean);
        PageResult pageResult = permissionService.findByPage(queryPageBean);

        return pageResult;

    }


    /**
     * 查询所有权限
     *
     * @return
     */
    //    "/permission/findAll.do"
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {
        List<Permission> list =null;
        try {
             list=permissionService.findAll();

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询失败");
        }
            return new Result(true, "查询成功", list);

    }

    /**
     * 查询角色的权限id集合
     *
     * @param id
     * @return
     */
//    "/permission/findPermissionIdsByRoleId.do?id=" + row.id
    @RequestMapping(path = "/findPermissionIdsByRoleId.do")
    public Result findCheckItemIdsByCheckGroupId(int id) {
        List<Integer> ids =null;
        try {
             ids=permissionService.findPermissionIdsByRoleId(id);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询权限id列表失败");
        }
            return new Result(true, "查询权限id列表成功", ids);

    }

}
