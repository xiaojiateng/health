package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Address;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.AddressService;
import com.itheima.service.CheckItemService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/address")
public class AddressController {


    @Reference
    private AddressService addressService;

    /**
     * 地址分页
     * @param queryPageBean
     * @return
     */
    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {


        System.out.println(queryPageBean);
        PageResult pageResult = addressService.findByPage(queryPageBean);

        return pageResult;

    }

    /**
     * //删除地址
     * @param id
     * @return
     */
    //    "/checkitem/delete.do?id="+row.id

    @RequestMapping(path = "/delete.do")
    public boolean delete(int id) {

        try {
            addressService.delete(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }

    }


    /**
     * 地址添加
     * @param address
     * @return
     */
    @RequestMapping(path = "/add.do")
    public Result add(@RequestBody Address address) {

        try {
            addressService.add(address);


        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "失败");
        }
        return new Result(true, "成功");
    }


    /**
     * 查询所有地址
     * @return
     */
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {
        List<Address> list=null;
        try {
             list= addressService.findAll();

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "查询地址失败");
        }
        return new Result(true, "查询地址成功", list);

    }
}
