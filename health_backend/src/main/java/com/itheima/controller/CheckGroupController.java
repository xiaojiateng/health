package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import com.itheima.service.CheckItemService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/checkgroup")
public class CheckGroupController {


    @Reference
    private CheckGroupService checkGroupService;

    /**
     * 查询所有检查组
     * @return
     */
    ///checkGroup/findAll.do
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {
        List<CheckGroup> list=null;
        try {
            list=checkGroupService.findAll();


        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,list);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    //    delete
    @RequestMapping(path = "/delete.do")
    public Result delete(int id) {

        try {
            checkGroupService.delete(id);


        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_CHECKGROUP_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_CHECKGROUP_SUCCESS);
    }

    /**
     * 编辑
     * @param checkGroup
     * @param ids
     * @return
     */
    //    edit
    @RequestMapping(path = "/edit.do")
    public Result edit(@RequestBody CheckGroup checkGroup, int[] ids) {

        try {
            checkGroupService.edit(checkGroup, ids);


        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_CHECKGROUP_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_CHECKGROUP_SUCCESS);
    }


    /**
     * 添加关系表
     * @param id
     * @return
     */
    //    findCheckItemIdsByCheckGroupId
    @RequestMapping(path = "/findCheckItemIdsByCheckGroupId.do")
    public Result findCheckItemIdsByCheckGroupId(int id) {
        List<Integer> ids =null;
        try {
             ids=checkGroupService.findCheckItemIdsByCheckGroupId(id);


        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, "查询检查项id列表失败");
        }
        return new Result(true, "查询检查项id列表成功", ids);
    }

    /**
     * 查询单个检查组
     * @param id
     * @return
     */
    //    findById
    @RequestMapping(path = "/findById.do")
    public Result findById(int id) {
        CheckGroup checkGroup =null;
        try {
             checkGroup=checkGroupService.findById(id);


        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_CHECKGROUP_FAIL);
        }
        return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS, checkGroup);
    }

    /**
     * 分页查询
     * @param queryPageBean
     * @return
     */
    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = checkGroupService.pageQuery(queryPageBean);
        return pageResult;

    }

    /**
     * 增加
     * @param checkGroup
     * @param ids
     * @return
     */
    //    add
    @RequestMapping(path = "/add.do")
    public Result add(@RequestBody CheckGroup checkGroup, int[] ids) {

        try {
            checkGroupService.add(checkGroup, ids);


        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
        }
        return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }


}
