package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Setmeal;
import com.itheima.service.CheckGroupService;
import com.itheima.service.SetMealService;
import com.itheima.utils.QiniuUtils;
import com.sun.jersey.api.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


//      @RestController包含了 @ResponseBody的返回json的功能
@RestController
@RequestMapping(path = "/setmeal")
public class SetMealController {
    @Autowired
    private JedisPool jedisPool;
    @Reference
    private SetMealService setMealService;

    //    /setmeal/findPage.do
    @RequestMapping(path = "/findPage.do")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean ) {
             PageResult pageResult=setMealService.findPage(queryPageBean);

             return  pageResult;

    }


    ///setmeal/add.do?ids="+this.checkgroupIds,this.formData
    @RequestMapping(path = "/add.do")
    public Result add(int[] ids, @RequestBody Setmeal setmeal) {
        try {
            setMealService.add(ids, setmeal);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
        return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS);


    }

    //    /setmeal/upload.do
    @RequestMapping(path = "/upload.do")
//    @RequestParam("imgFile") MultipartFile imgFile
    public Result upload(@RequestParam("imgFile") MultipartFile imgFile) {
        System.out.println(imgFile);
        String originalFilename = imgFile.getOriginalFilename();//原始文件名 3bd90d2c-4e82-42a1-a401-882c88b06a1a2.jpg
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = sdf.format(new Date());//将时间川作为图片名

        int lastpoint = originalFilename.lastIndexOf(".");
        String suffix = originalFilename.substring(lastpoint);

        String fileName = date + suffix;//时间+后缀=全名称

        try {
 //方法一:  **********************************************************
            //将文件上传到七牛云服务器
            QiniuUtils.upload2Qiniu(imgFile.getBytes(), fileName);

 // 方法二：**********************************************************
  //---》》》自己搭建图片服务器
//           使用com.sun.jersey.api.client.Client类建立远程连接
            /*Client client = Client.create();
            //设置上传到图片服务器的upload目录下
            String path = "http://localhost:8080/fileServer/upload/";
            //上传步骤
            //1.path + fileName   此图片在图片服务器上边的位置
            //2.imgFile.getBytes()   将此图片转成字节数组
            //上传结束
            client.resource(path + fileName).put(imgFile.getBytes());*/

            //将图片存进redis缓存,基于Redis的Set集合存储
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
        return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS, fileName);

    }

    //    新增请求窗口展示套餐信息
    @RequestMapping("/findAll.do")
    public Result findAll(){
        try {
            List<Setmeal> list = setMealService.findAll();
            return new Result(true,MessageConstant.GET_SETMEAL_LIST_SUCCESS,list);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,MessageConstant.GET_SETMEAL_LIST_FAIL);
    }

//    用于编辑回显
    @RequestMapping("/findOnlySetmealById.do")
    public Result findById(int id){
        try {
            Setmeal byId = setMealService.findOnlySetmealById(id);
            return new Result(true,"查询套餐成功",byId);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,"查询套餐失败");
    }
//  用于回显复选框
    @RequestMapping("/findGroupById.do")
    public Result findGroupById(int id){
        List<Integer> ids =null;
        try {
            ids = setMealService.findGroupById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(true,"",ids);
    }

//    编辑检查组
    @RequestMapping("/edit.do")
    public Result Edit(@RequestBody Setmeal setmeal, Integer[] ids){
        try{
            setMealService.edit(ids,setmeal);
            return new Result(false, "编辑套餐成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(true, "编辑套餐失败");
    }

//    删除套餐
    @RequestMapping("/delete.do")
    public Result deleteById(int id){

        try {
            boolean flag = setMealService.deleteById(id);
            if(flag)return new Result(true,"删除成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return new Result(false,"删除失败");
    }
}
