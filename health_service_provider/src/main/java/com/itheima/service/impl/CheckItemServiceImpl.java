package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {
    @Autowired
    private CheckItemDao checkItemDao;



    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    @Override
    public PageResult findByPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString =queryPageBean.getQueryString();//查询条件

        //完成分页查询，基于mybatis框架提供的分页助手插件完成


        //pagehelper工作流程：使用当前页和每页显示条数开启分页【将对接下来的一个查询进行分页，不能是增删改操作】
        //dao层通过查询条件(如果有)，将查询到的数据交给pagehelper提供的托管类page(本质是list)
        //然后可以通过page.getresult()得到当前页的数据，通过page.gettotal()得到总记录数

        PageHelper.startPage(currentPage,pageSize);
        //select * from t_checkitem limit 0,10
        //复杂点的分页方法
        /*List<CheckItem> list=checkItemDao.selectByCondition(queryString);
        PageInfo<CheckItem> pageInfo = new PageInfo<>(list);
        pageInfo.getTotal();
        pageInfo.getList();*/
        //简单点的分页方法
        Page<CheckItem> page = checkItemDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<CheckItem> rows = page.getResult();
        return new PageResult(total,rows);
    }

    @Override
    public void delete(int id) {
        checkItemDao.delete(id);
    }

    @Override
    public CheckItem findById(int id) {

        return checkItemDao.findById(id);
    }

    @Override
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }

    @Override
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }
}
