package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 用户服务
 */
@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    /**
     * 根据用户名查询数据库获取用户信息和关联的角色信息，同时需要查询角色关联的权限信息
     * @param username
     * @return
     */
    //
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);//查询用户基本信息，不包含用户的角色
        if(user == null){
            return null;
        }
        Integer userId = user.getId();
        //根据用户ID查询对应的角色
        Set<Role> roles = roleDao.findByUserId(userId);

        for (Role role : roles) {
            Integer roleId = role.getId();
            //根据角色ID查询关联的权限
            Set<Permission> permissions = permissionDao.findByRoleId(roleId);
            role.setPermissions(permissions);//让角色关联权限
        }
        user.setRoles(roles);//让用户关联角色
        return user;
    }

    /**
     * 分页信息
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();//当前页数
        Integer pageSize = queryPageBean.getPageSize();//页面显示条数
        String queryString = queryPageBean.getQueryString();//模糊查询条件

        PageHelper.startPage(currentPage,pageSize);

        Page<User> page = userDao.findByConDition(queryString);
        long total = page.getTotal();
        List<User> rows = page.getResult();
        return new PageResult(total,rows);
    }

    /**
     * 添加用户
     * @param user
     * @param ids
     */
    @Override
    public void add(User user, int[] ids) {
        //首先添加用户信息
        userDao.add(user);
        //根据添加完成返回的id,建立用户与角色的关系列表
        Integer userId = user.getId();
        //创建用户跟角色表的关系
        if (ids != null && ids.length > 0 ){
            for (int roleId : ids) {
                //直接将用户id和角色id传到dao添加
                userDao.setUserAndRole(userId,roleId);
            }
        }
    }

    /**
     * 查询单个用户
     * @param id
     * @return
     */
    @Override
    public User findById(int id) {
        return userDao.findById(id);
    }


    @Override
    public List<Integer> findRoleById(int id) {
        return userDao.findRoleById(id);
    }

    /**
     * 编辑用户
     * @param ids
     * @param user
     */
    @Override
    public void edit(Integer[] ids, User user) {
        //1 修改用户基本信息
        userDao.edit(user);
        //2 修改用户角色信息
        userDao.delete(user.getId());
        setUserAndRole(user.getId(),ids);
    }

    /**
     * 删除用户
     * @param id
     */
    //
    @Override
    public void delete(Integer id) {
        //删除用户角色中间关系数据
        userDao.delete(id);
        //删除该用户信息
        userDao.deleteUserByUserId(id);

    }

    /**
     * 设置用户和角色的关联关系
     * @param id
     * @param roleIds
     */
    //
    private void setUserAndRole(Integer id, Integer[] roleIds) {
          if (roleIds != null && roleIds.length > 0){
              for (Integer roleId : roleIds) {
                  userDao.setUserAndRole(id,roleId);
              }
          }
    }
}
