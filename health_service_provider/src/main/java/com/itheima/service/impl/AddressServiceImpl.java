package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.AddressDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Address;
import com.itheima.pojo.CheckItem;
import com.itheima.service.AddressService;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = AddressService.class)
@Transactional
public class AddressServiceImpl  implements AddressService  {

    @Autowired
    private AddressDao addressDao;


    /**
     * 地址分页查询
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findByPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString =queryPageBean.getQueryString();//查询条件

        PageHelper.startPage(currentPage,pageSize);

        //简单点的分页方法
        Page<Address> page = addressDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<Address> rows = page.getResult();
        return new PageResult(total,rows);
    }

    /**
     * 地址删除
     * @param id
     */
    @Override
    public void delete(int id) {
         addressDao.delete(id);
    }

    /**
     * 查询所有地址
     * @return
     */
    @Override
    public List<Address> findAll() {
        return addressDao.findAll();
    }

    /**
     * 添加地址
     * @param address
     */
    @Override
    public void add(Address address) {
        addressDao.add(address);
    }
}
