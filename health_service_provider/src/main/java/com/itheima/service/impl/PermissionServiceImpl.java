package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.service.PermissionService;
import com.itheima.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;


    /**
     * 查询所有权限
     * @return
     */
    @Override
    public List<Permission> findAll() {
        return permissionDao.findAll();
    }

    /**
     * 通过角色id查询权限id集合
     * @param id
     * @return
     */
    @Override
    public List<Integer> findPermissionIdsByRoleId(int id) {
        return permissionDao.findPermissionIdsByRoleId(id);
    }

    /**
     * 分页查询权限信息
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findByPage(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString =queryPageBean.getQueryString();//查询条件
        PageHelper.startPage(currentPage,pageSize);

        Page<Permission> page = permissionDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<Permission> rows = page.getResult();
        return new PageResult(total,rows);
    }

    /**
     * 添加权限
     * @param permission
     */
    @Override
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    /**
     * 查询单个权限
     * @param id
     * @return
     */
    @Override
    public Permission findById(int id) {
        return permissionDao.findById(id);
    }

    /**
     * 编辑权限
     * @param permission
     */
    @Override
    public void edit(Permission permission) {
        permissionDao.edit(permission);
    }

    /**
     * 删除权限
     * @param id
     */
    @Override
    public void delete(int id) {
        permissionDao.delete(id);
    }


}
