package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.RoleDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    /**
     * 分页查询角色
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        //        获取三个参数
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        //开启分页
        PageHelper.startPage(currentPage,pageSize);
        //查询并赋值给Page托管类
        Page<Role> page=roleDao.findByCondition(queryString);
        long total = page.getTotal();
        List<Role> result = page.getResult();
        return new PageResult(total,result);
    }

    /**
     * 添加角色
     * @param role
     * @param ids
     */
    @Override
    public void add(Role role, int[] ids) {
        //先添加组基本信息
        roleDao.add(role);
        //根据添加后返回的id ,来建立组和项的关系列表
        Integer roleId = role.getId();
        System.out.println("roleId = " + roleId);
        //创建关系map
        if(ids != null && ids.length > 0){
            for (Integer permissionId : ids) {
                roleDao.setRoleAndPermission( roleId,permissionId);

            }
        }
    }

    /**
     * 查询单个角色信息
     * @param id
     * @return
     */
    @Override
    public Role findById(int id) {
        return roleDao.findById(id);
    }

    /**
     * 编辑角色
     * @param role
     * @param ids
     */
    @Override
    public void edit(Role role, int[] ids) {
        Integer roleId = role.getId();
        //修改检查组表
        roleDao.edit(role);
        //先把中间表的旧关系删除
        roleDao.deleteAssocication(roleId);
        //建立新的关系记录

        if(ids!=null && ids.length>0){
            for (int permissionId : ids) {

                roleDao.setRoleAndPermission(roleId,permissionId);
            }
        }

    }

    /**
     * 删除角色
     * @param id
     */
    @Override
    public void delete(int id) {
        //删除关系表的记录
        roleDao.deleteAssocication(id);
        //删除角色的记录
        roleDao.delete(id);
    }

    /**
     * 查询所有角色信息
     * @return
     */
    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }
}
