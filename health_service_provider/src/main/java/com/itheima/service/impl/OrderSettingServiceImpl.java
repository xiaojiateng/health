package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.RedisConstant;
import com.itheima.dao.OrderSettingDao;
import com.itheima.dao.SetMealDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.OrderSetting;
import com.itheima.pojo.Setmeal;
import com.itheima.service.OrderSettingService;
import com.itheima.service.SetMealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.*;

@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingDao orderSettingDao;


    @Override
    public void add(ArrayList<OrderSetting> list) {
        if(list != null && list.size() > 0) {
            for (OrderSetting orderSetting : list) {
                long countByOrderDate = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
                System.out.println("订单时间数量：  " + countByOrderDate);
                //判断当前日期是否已经进行了预约设置
                if (countByOrderDate > 0) {
                    //已经进行了预约设置，执行更新操作
                    orderSettingDao.editNumberByOrderDate(orderSetting);
                }else {
                    //没有进行预约设置，执行插入操作
                    orderSettingDao.add(orderSetting);
                }
            }

        }
    }

    @Override
    public List<Map> getOrderSettingByMonth(String date) {
        //格式为  2019-03
        //该月的月头和月尾是：
        String beginDate=date+"-01";
        String endDate=date+"-31";
        //传入dao，查询该月所有的预设记录
        List<OrderSetting> orderSettingList=orderSettingDao.findOrderSettingByMonth(beginDate,endDate);
        List<Map> list = new ArrayList<>();

        for (OrderSetting orderSetting : orderSettingList) {
            Map map = new HashMap<>();
            map.put("date",orderSetting.getOrderDate().getDate());//获得日期（第几号）
            map.put("number",orderSetting.getNumber());//人数上限
            map.put("reservations",orderSetting.getReservations());//已订人数

            list.add(map);
        }

        return list;
    }

    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        Date orderDate = orderSetting.getOrderDate();
        //根据日期查询是否已经进行了预约设置
        long count = orderSettingDao.findCountByOrderDate(orderDate);
        if(count > 0){
            //当前日期已经进行了预约设置，需要执行更新操作
            orderSettingDao.editNumberByOrderDate(orderSetting);
        }else{
            //当前日期没有就那些预约设置，需要执行插入操作
            orderSettingDao.add(orderSetting);
        }
    }
}
