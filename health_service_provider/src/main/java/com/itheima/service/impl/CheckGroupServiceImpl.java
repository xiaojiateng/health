package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckGroupService;
import com.itheima.service.CheckItemService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;
    @Override
    public void add(CheckGroup checkGroup, int[] ids) {
        //先添加组基本信息
        checkGroupDao.add(checkGroup);
        //根据添加后返回的id ,来建立组和项的关系列表
        Integer checkgroupId = checkGroup.getId();
        //创建关系map
        if(ids != null && ids.length > 0){
            for (Integer checkitemId : ids) {
                //map集合存关系表的一对关系会明了一些
                /*Map<String,Integer> map = new HashMap<>();
                map.put("checkgroupId",checkgroupId);
                map.put("checkitemId",checkitemId);
                checkGroupDao.setCheckGroupAndCheckItem(map);*/
                //直接将组id和项id传到dao，在dao方法中加上注解@param("xxx"),可以直接在mapper.xml的statment根据#{xxx}拿到参数，并建立关系记录
                checkGroupDao.setCheckGroupAndCheckItem( checkgroupId,checkitemId);

            }
        }

    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString =queryPageBean.getQueryString();//查询条件

        PageHelper.startPage(currentPage,pageSize);

        Page<CheckGroup> page = checkGroupDao.findByCondition(queryString);
        long total = page.getTotal();
        List<CheckGroup> rows = page.getResult();
        return new PageResult(total,rows);
    }

    @Override
    public CheckGroup findById(int id) {
        return checkGroupDao.findById(id);
    }

    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(int id) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    @Override
    public void edit(CheckGroup checkGroup, int[] ids) {
        Integer checkGroupId = checkGroup.getId();
        //修改检查组表
        checkGroupDao.edit(checkGroup);
        //先把中间表的旧关系删除
        checkGroupDao.deleteAssocication(checkGroupId);
        //建立新的关系记录

        if(ids!=null && ids.length>0){
            for (int checkitemId : ids) {

                checkGroupDao.setCheckGroupAndCheckItem(checkGroupId,checkitemId);
            }
        }

    }

    @Override
    public void delete(int id) {
        //删除关系表的记录
        checkGroupDao.deleteAssocication(id);
        //删除检查组的记录
        checkGroupDao.delete(id);
    }

    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }
}
