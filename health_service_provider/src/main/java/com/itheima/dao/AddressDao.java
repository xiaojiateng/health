package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Address;
import com.itheima.pojo.CheckItem;

import java.util.List;

public interface AddressDao {
    Page<Address> selectByCondition(String queryString);

    void delete(int id);

    List<Address> findAll();

    void add(Address address);
}
