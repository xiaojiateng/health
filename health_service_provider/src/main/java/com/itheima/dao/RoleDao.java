package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface RoleDao {

    Page<Role> findByCondition(String queryString);

    void add(Role role);

    void setRoleAndPermission(@Param("roleId")Integer roleId, @Param("permissionId")Integer permissionId);

    Role findById(int id);

    void edit(Role role);

    void deleteAssocication(Integer roleId);

    public Set<Role> findByUserId(Integer userId);

    void delete(int id);

    List<Role> findAll();
}
