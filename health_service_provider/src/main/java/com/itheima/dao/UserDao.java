package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    public User findByUsername(String username);

    Page<User> findByConDition(String queryString);

    void add(User user);

    void setUserAndRole(@Param("userId") Integer userId,@Param("roleId") int roleId);

    User findById(int id);

    List<Integer> findRoleById(int id);

    void edit(User user);

    void delete(Integer id);

    void deleteUserByUserId(Integer id);
}
