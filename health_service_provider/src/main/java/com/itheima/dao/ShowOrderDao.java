package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.ShowOrder;

public interface ShowOrderDao {
    Page<ShowOrder> findPage(String queryString);

    void cancel(int id);
}
