package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PermissionDao {


    List<Permission> findAll();


    List<Integer> findPermissionIdsByRoleId(int id);

    public Set<Permission> findByRoleId(Integer roleId);

    Page<Permission> selectByCondition(String queryString);

    void add(Permission permission);

    Permission findById(int id);

    void edit(Permission permission);

    void delete(int id);
}
