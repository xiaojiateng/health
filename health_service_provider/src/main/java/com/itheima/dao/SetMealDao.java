package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Order;
import com.itheima.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SetMealDao {
    void add(Setmeal setmeal);

    void setSetmealAndCheckgroup(@Param("setmealId") int setmealId, @Param("checkgroupId") int checkgroupId);

    Page<Setmeal> findPage(String queryString);

    List<Setmeal> findAll();

    Setmeal findById(int id);

    List<Map<String, Object>> findSetmealNameAndCount();

    void edit(Setmeal setmeal);

    void deleteAssociation(Integer id);

    void deleteById(int id);

    List<Order> findIsOrderedBySetmealId(int setmealId);

    List<Integer> findGroupById(int id);

    Setmeal findOnlySetmealById(int id);
}
