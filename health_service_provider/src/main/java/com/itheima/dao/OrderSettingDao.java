package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.OrderSetting;
import com.itheima.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderSettingDao {

    long findCountByOrderDate(Date orderDate);

    void editNumberByOrderDate(OrderSetting orderSetting);

    void add(OrderSetting orderSetting);

    List<OrderSetting> findOrderSettingByMonth(@Param("beginDate") String beginDate, @Param("endDate")String endDate);

    OrderSetting findByOrderDate(Date date);

    void editReservationsByOrderDate(OrderSetting orderSetting);
}
