package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.service.MemberService;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * 套餐管理
 */

@RestController
@RequestMapping("/member")
public class MemberController {
    @Autowired
    private JedisPool jedisPool;
    @Reference
    private MemberService memberService;

//    "/member/login.do",this.loginInfo


    @RequestMapping("/login.do")
    public Result getAllSetmeal(HttpServletResponse response, @RequestBody Map map) {


        String validateCode = (String) map.get("validateCode");
        String telephone = (String) map.get("telephone");
        String codeInRedis = jedisPool.getResource().get(telephone+RedisMessageConstant.SENDTYPE_LOGIN
        );
        if(codeInRedis==null || !codeInRedis.equalsIgnoreCase(validateCode)){

            //验证码不通过
            return new Result(false,MessageConstant.VALIDATECODE_ERROR);
        }else {
            //验证码输入正确
            //判断当前用户是否为会员

            Member member = memberService.findByTelephone(telephone);
            if(member==null){
                member = new Member();
                member.setPhoneNumber(telephone);
                member.setRegTime(new Date());
                //注册
                memberService.add(member);
            }


            //登录成功
            //写入Cookie，跟踪用户
            Cookie cookie = new
                    Cookie("login_member_telephone",telephone);
            cookie.setPath("/");//路径
            cookie.setMaxAge(60*60*24*30);//有效期30天
            response.addCookie(cookie);
            //保存会员信息到Redis中
            String json = JSON.toJSON(member).toString();
            jedisPool.getResource().setex(telephone,60*30,json);
            return new Result(true,MessageConstant.LOGIN_SUCCESS);



        }






    }

}
