package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Address;
import com.itheima.pojo.Order;
import com.itheima.pojo.Setmeal;
import com.itheima.service.AddressService;
import com.itheima.service.OrderService;
import com.itheima.service.SetMealService;
import com.itheima.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Map;

/**
 * 套餐管理
 */

@RestController
@RequestMapping("/order")
public class OrderController {
    private Result result;
    @Autowired
    private JedisPool jedisPool;

    @Reference
    private OrderService orderService;
    @Reference
    private AddressService addressService;

    /**
     * 体检预约提交
     * @param map
     * @return
     */
    //    "/order/submit.do",this.orderInfo
    //
    @RequestMapping("/submit.do")
    public Result getAllSetmeal(@RequestBody Map map) {
        String telephone = (String) map.get("telephone");
        //从redis中取出验证码
        String codeInRedis = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_ORDER);
        //用户输入的验证码
        String validateCode = (String) map.get("validateCode");
        //校验缓存验证码和用户输入验证码是否一致
        if (codeInRedis == null || !codeInRedis.equalsIgnoreCase(validateCode)) {
            return new Result(false, MessageConstant.VALIDATECODE_ERROR);
        }

        Result result = null;
        //调用体检预约服务

        try {
            map.put("orderType", Order.ORDERTYPE_WEIXIN);
            result = orderService.order(map);
        } catch (Exception e) {
            e.printStackTrace();
            //预约失败
            return result;
        }

        if (result.isFlag()) {
            //预约成功，发送短信通知
            String orderDate = (String) map.get("orderDate");
            try {
                SMSUtils.sendShortMessage(SMSUtils.ORDER_NOTICE, telephone, orderDate);
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }
        return result;

    }


    /**
     * 查询预约
     * @param id
     * @return
     */
    //    "/order/findById.do?id=" + id
    @RequestMapping("/findById.do")
    public Result getAllSetmeal(int id) {

        try {
            Map map=orderService.findById(id);
            return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);

        }


    }

    /**
     *查询所有地址
     * @return
     */
    @RequestMapping(path = "/findAll.do")
    public Result findAll() {

        try {
            List<Address> list = addressService.findAll();
            result = new Result(true, "查询地址成功", list);
        } catch (Exception e) {
            e.printStackTrace();
            result = new Result(false, "查询地址失败");
        }
        return result;

    }
}
