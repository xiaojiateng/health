package com.itheima.service;

import com.itheima.pojo.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {


    Member findByTelephone(String telephone);

    void add(Member member);

    public List<Integer> findMemberCountByMonths(List<String> months);


}
