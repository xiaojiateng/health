package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;

import java.util.List;

public interface PermissionService {


    List<Permission> findAll();


    List<Integer> findPermissionIdsByRoleId(int id);

    PageResult findByPage(QueryPageBean queryPageBean);

    void add(Permission permission);

    Permission findById(int id);

    void edit(Permission permission);

    void delete(int id);
}
