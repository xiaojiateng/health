package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;

import java.util.Map;

public interface OrderService {


    Result order(Map map) throws Exception;

    Map findById(int id) throws Exception;

    Result orderByComputer(Order newOrder, String phoneNumber, String name);

    void cancel(int id);

    PageResult findPage(QueryPageBean queryPageBean);

    void sendMessage(int orderId, String type);
}
