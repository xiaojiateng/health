package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    PageResult pageQuery(QueryPageBean queryPageBean);

    void add(User user, int[] ids);

    User findById(int id);

    List<Integer> findRoleById(int id);

    void edit(Integer[] ids, User user);

    void delete(Integer id);
}
