package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Address;

import java.util.List;

public interface AddressService {
    PageResult findByPage(QueryPageBean queryPageBean);

    void delete(int id);

    List<Address> findAll();

    void add(Address address);
}
