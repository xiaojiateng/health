package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Role;

import java.util.List;

public interface RoleService {


    PageResult pageQuery(QueryPageBean queryPageBean);

    void add(Role role, int[] ids);

    Role findById(int id);

    void edit(Role role, int[] ids);

    void delete(int id);

    List<Role> findAll();
}
