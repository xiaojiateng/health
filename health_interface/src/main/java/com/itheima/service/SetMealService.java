package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetMealService {


    void add(int[] ids, Setmeal setmeal);

    PageResult findPage(QueryPageBean queryPageBean);

    List<Setmeal> findAll();

    List<Map<String, Object>> findSetmealNameAndCount();

    void edit(Integer[] ids, Setmeal setmeal);

    boolean deleteById(int id);

    List<Integer> findGroupById(int setmeal_id);

    Setmeal findOnlySetmealById(int id);

}
