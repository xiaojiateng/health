package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

public interface CheckItemService {
    void add(CheckItem checkItem);

    PageResult findByPage(QueryPageBean queryPageBean);

    void delete(int id);

    CheckItem findById(int id);

    void edit(CheckItem checkItem);

    List<CheckItem> findAll();
}
