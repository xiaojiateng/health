package com.itheima.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpringJobs {
    public static void main(String[] args) {
    new ClassPathXmlApplicationContext("applicationContext-spring_jobs.xml");

    }
}
