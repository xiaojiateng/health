package com.itheima.jobs;

import com.itheima.constant.RedisConstant;
import com.itheima.utils.QiniuUtils;
import com.sun.jersey.api.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * 自定义Job，实现定时清理垃圾图片
 */
@Component("clearImgJob")
public class ClearImgJob {
    @Autowired
    private JedisPool jedisPool;
    public void clearImg(){
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) +",定时任务触发。。。");
        //           使用com.sun.jersey.api.client.Client类建立远程连接
        Client client = Client.create();
        //设置路径为图片服务器的upload目录下
        String path = "http://localhost:8080/fileServer/upload/";

        //根据Redis中保存的两个set集合进行差值计算，获得垃圾图片名称集合
        Set<String> set = jedisPool.getResource().sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        if(set != null){
            for (String picName : set) {
//方法一：**************************************************************
                /*//删除七牛云服务器上的图片
                QiniuUtils.deleteFileFromQiniu(picName);*/

//方法二： ***********************************************************************
//                删除图片服务器上的图片
                //删除步骤
                //1.path + fileName   此图片在图片服务器上边的位置
                //上传结束
                client.resource(path + picName).delete();
                //从Redis集合中删除图片名称
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,picName);
                System.out.println("发现垃圾图片，清理垃圾图片:" + picName);

            }
        }


        System.out.println("***********************************************************************");
    }
}
