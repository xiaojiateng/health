package com.itheima.springjobs;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*spring自带的定时器：相比于quartz定时任务调度,功能弱一点，cron表达式也只支持六位(没有‘年’)*/
@Component
@EnableScheduling
public class SpringJobsTest {
    @Scheduled(cron = "0/10 * * * * ?")
    public void run(){
        System.out.println(" SpringJobsTest的run()执行了。。。。。");
    }
}
